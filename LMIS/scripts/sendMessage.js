var sendMessage = (function ( ) {
    //private variables goes here
    //private methods decleared here
    var buildOrderJson = function () {
        var message = getCommodityString();
        var comment = $('#commentTxtArea').val();
        var recipientId = SendFormValidation.getRecipientId().id;
        var allUsersChk = $('#allUsersChk');
        var sendTo = [{
                "id": recipientId
            }];

        if (comment != "Add comment to order message (optional)")
            message += "\n\nComment: " + comment;

        var order = {
            "subject": "LMIS order form",
            "text": message
        };

        if(SendFormValidation.getRecipientId().type == "orgunit"){
            order.organisationUnits = sendTo;
        }
        else if(SendFormValidation.getRecipientId().type == "person"){
            order.users = sendTo;
        }

        return order;

        /*
        if (orgUnit == "Select orgunit") {
            alert("Need to choose orgunit!");
            return false;
        }
        else if (allUsersChk.is(':checked')) {

        }
        else if (user == "Select recipient (optional)") {
            var r = confirm("You did not choose a user to send to. Message will be sent to all users in selected organisation unit. \n Is that acceptable?");
            if (r == true) {
                sendTo = [{
                    "id": orgUnit
                }];
                order.organisationUnits = sendTo;
            }
            else
                return false;
        }
        else {
            sendTo = [{
                "id": "" + user
            }];
            order.users = sendTo;
        }
        */
    };
    
    var getCommodityString = function () {
        var commForm = $("#commForm");
        var arr = commForm.serializeArray();
        var commArray = [];

        /*While there are (triplets of) elements in the form, we create a commodity object with name, current stock and stock need.
         After construction we add this object to the list of commodities.
         */
        while (arr.length > 0) {
            commArray.push(new Commodity(arr.shift().value, arr.shift().value, arr.shift().value));
        }


        var jsonString = JSON.stringify(commArray);
        console.log(jsonString);

        var message = "Order form from LMIS\nList of commodities will follow: \n \n";
        while (commArray.length > 0) {
            var commodity = commArray.shift()
            /*If there is an empty field we do not add this field to the message, altho this should've been fixed
            * ruled out because of the validation function, this is just an extra fail safe.*/
            if (commodity.Commodity == "" && commodity.CurrentStock == "" && commodity.NeedStock == "")
                continue;
            else
                message += "Commodity: " + commodity.Commodity + ", Current stock: " + commodity.CurrentStock + ", Number ordered: " + commodity.NeedStock + "\n";
        }
        ;
        return message;
    }
    
    var sendMessage = function () {
        var order = buildOrderJson();
        if (order == false)
            return;
        /*
         Send constructed message to user or organisation unit in the DHIS2 system.
         */
        ApiLoader.apiBaseUrl().done(function(baseUrl) {
            var request = $.ajax({
                type: 'POST',
                url:baseUrl + 'messageConversations',
                dataType: 'json',
                contentType: 'application/json',
                crossDomain: true,
                data: JSON.stringify(order)
            });
            request.done(function(data, textStatus, jqXHR) {
                $('#modalMessageContent').empty();
                $('#modalMessageStatus').empty();
                $('#modalMessageStatus').append("<b>Message Status: </b>" + textStatus);
                $('#modalMessageContent').append(order.text);
                $('#messageConfirmationModal').modal('show');
            });
            request.error(function( jqXHR, status, error) {
                console.log(status+": " +error);
            });    
        });
    };
    
    //Commmodity constructor
    var Commodity = function (Commodity, CurrentStock, NeedStock) {
        this.Commodity = Commodity;
        this.CurrentStock = CurrentStock;
        this.NeedStock = NeedStock;
    };
    
    //control bindings
    $('#submitBtn').click(function(e) {
        e.preventDefault();
        if (SendFormValidation.validateForm() == true) {
            sendMessage();
        }
    });
    //public interface goes here
    return {
        
    };
})();