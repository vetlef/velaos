/*$('#submitBtn').click(function(e) {
    e.preventDefault();
    dataStorePost();
});
$('#orderlistButton ').click (function(){
	dataStoreGet();
});
*/
function dataStorePost() {
    var orderlist = getCommodityString();
    /*
    This is HTTP POST request to change with the datastore for the app. It specifically changes the item with key "commodity_list"
     */
    ApiLoader.apiBaseUrl().done(function(baseUrl) {
        var request = $.ajax({
            type: 'POST',
            url:baseUrl + 'dataStore/LMIS_velaos/commodity_list',
            dataType: 'json',
            contentType: 'application/json',
            crossDomain: true,
            data: JSON.stringify(orderlist)
        });
        request.done(function(data, textStatus, jqXHR) {
            alert("Success");
        });
        request.error(function( jqXHR, status, error) {
            console.log(status+": " +error);
        });
    });
}

function getCommodityString() {
    var commForm = $("#commForm");
    var arr = commForm.serializeArray();
    var commArray = [];

    /*While there are (triplets of) elements in the form, we create a commodity object with name, current stock and stock need.
     After construction we add this object to the list of commodities.
     */
    while (arr.length > 0) {
        commArray.push(new Commodity(arr.shift().value, arr.shift().value, arr.shift().value));
    }


    var jsonString = JSON.stringify(commArray);
    console.log(jsonString);

    var orderlist = '{'+ date()+ ':[';
    while (commArray.length > 0) {
        var commodity = commArray.shift()
        /*If there is an empty field we do not add this field to the message, altho this should've been fixed
        * ruled out because of the validation function, this is just an extra fail safe.*/
        if (commodity.Commodity == "" && commodity.CurrentStock == "" && commodity.NeedStock == "")
            continue;
        else
         orderlist += '{"Commodity": ' + commodity.Commodity + ', "Current stock": ' +
         commodity.CurrentStock + ', "Number ordered": ' + commodity.NeedStock +
         '},';
    };
    orderlist += ']}';
    return orderlist;
}

//Commmodity constructor
function Commodity(Commodity, CurrentStock, NeedStock) {
    this.Commodity = Commodity;
    this.CurrentStock = CurrentStock;
    this.NeedStock = NeedStock;
}


function dataStoreGet() {
    /*
     This is HTTP GET request to get data from the datastore for the app. It specifically fetches the item with key "commodity_list"
     */
    ApiLoader.apiBaseUrl().done(function(baseUrl) {
        var request = $.ajax({
            type: 'GET',
            url:baseUrl + 'dataStore/LMIS_velaos/commodity_list',
            contentType: 'application/json',
        });
        request.done(function(data, textStatus, jqXHR) {
            alert("Success" + JSON.stringify(data));
        });
        
        

        request.error(function( jqXHR, status, error) {
            console.log(status+": " +error);
        });
    });
}

