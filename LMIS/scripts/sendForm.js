var SendForm = (function () {
        //private variables
        var orgUnitSelect = $("#orgunitSelect");
        var personsSelect = $("#personsSelect");
        var receiverInput = $("input[name='receiverInput']");
        var orgunitsLoaded = false;
        var prevSearchResult = null;
        var higherManagement = null;
        //private method
        var putArrayInSelect = function (selectElement, array, emptyFirst, messageInFirstElement, emptyMessage) {
                if(emptyFirst)selectElement.empty();
                if(array.length == 0) messageInFirstElement = emptyMessage;
                if(messageInFirstElement != null)selectElement.append($("<option/>").attr("value","#00").text(messageInFirstElement));
                $.each(array,function(index, value){
                     selectElement.append($("<option/>").attr("value",value.id).text(value.name));
                });
        };
        var loadHigherManagement = function ( ) {
                if(orgunitsLoaded)return;
                ApiLoader.getMeJSON().done(function (me) {
                var idOfOrganisation = me.organisationUnits[0].id;
                ApiLoader.getOrgUnit(idOfOrganisation).done(function (orgUnit) {
                    parentOrgunits = orgUnit.parents.reverse();
                    putArrayInSelect(orgUnitSelect, parentOrgunits, true, "Select orgunit", "No org units where found");
                    higherManagement = orgUnit.parents;
                    isOrgunitsOfUserLoaded = true;
                });
            });
        };
        //controll bindings
        orgUnitSelect.on("change",function ( ) {
            if(!(orgUnitSelect.val() == "#00")){
                var idOfSelectedOrgUnit = orgUnitSelect.val();
                ApiLoader.getUsersOfOrgunit(idOfSelectedOrgUnit).done(function (users) {
                var user = users.users;
                putArrayInSelect(personsSelect,user,true,"Select recipient (optional)", "No persons where found in selected orgunit");
                });  
            }   
        });
        $("#searchBtn").click(function ( e ) {
            var name = $("#searchInput").val();
            ApiLoader.getUserByName(name).done(function (result){ 
                prevSearchResult = result.users;
                putArrayInSelect($("#personsSearchSelect"),result.users,true,null,"No persons was found with that name");
            });
            e.preventDefault();
        });
        $("#personsSearchSelect").on("change",function(  ){
/*           var selectedValue = $("#personsSearchSelect").val();
             //find person in last search!
             var orgunitOfPerson = null;
             for(var i in prevSearchResult){
                 if(prevSearchResult[i].id == selectedValue){
                    orgunitOfPerson = prevSearchResult[i].organisationUnits[0]; 
                    //search for person in highermanagement
                    for(var h in higherManagement){
                        if(higherManagement[h] == orgunitOfPerson.id){
                             alert("person is in higherManagement");
                             return true;
                        }
                    }
                 }
             }
            return false;
        */
        });
        //init
        loadHigherManagement();
        //public interface
        return {
            loadHigherManagement : loadHigherManagement
        };
    })();