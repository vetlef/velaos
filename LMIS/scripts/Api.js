var ApiLoader = (function ( ){ 
        //private variables
        var baseApiUrl;
        var baseUrl;
        var me;
        //private methods
        //gets the base url and returns a string
        var getBaseUrl = function ( ) {
            if(baseUrl == null){
               return $.getJSON('manifest.webapp').then(function(json, status, jqXHR){
                   baseUrl = json.activities.dhis.href;
                   var deffered = $.Deferred();
                   deffered.resolve(baseUrl);
                   return deffered.promise();
               });
            }
            var deffered = $.Deferred();
            deffered.resolve(baseUrl);
            return deffered.promise();
        };
        //Gets the base apiUrl and returns a string
        var getBaseApiUrl = function ( ) {
            if(baseApiUrl == null){
               return getBaseUrl().then(function(baseurl){
                   baseApiUrl = baseurl + "/api/";
                   var deffered = $.Deferred();
                   deffered.resolve(baseApiUrl);
                   return deffered.promise();
               });
            }
            var deffered = $.Deferred();
            deffered.resolve(baseApiUrl);
            return deffered.promise();
        };
        //Get a organisation unit by id
        var loadOrgunitById = function (orgunitID) {
            if (baseApiUrl == null) {
                return getBaseApiUrl().then(
                             function( ) {
                                 return $.getJSON(baseApiUrl + "organisationUnits/" + orgunitID)
                             }, 
                             function ( ) {
                                 alert("failed to get manifest")
                             });
            }
            return $.getJSON(baseApiUrl + "organisationUnits/" + orgunitID);
        };
        var loadCurrentUsersOrgunit = function (orgunitID) {
            if (baseApiUrl == null) {
                return getBaseApiUrl().then(
                             function( ) {
                                 return $.getJSON(baseApiUrl + "organisationUnits/" + orgunitID)
                             }, 
                             function ( ) {
                                 alert("failed to get manifest")
                             });
            }
            return $.getJSON(baseApiUrl + "organisationUnits/" + orgunitID);
        };
        //Finds a user based on id
        var getUserById = function (userID) {
            if (baseApiUrl == null) {
                return getBaseApiUrl().then(function () {
                    return $.getJSON(baseApiUrl + "users/" + userID)
                });
            }
            return $.getJSON(function () {
                return $.getJSON(baseApiUrl + "users/" + userID)
            });
        };
        //Finds all users of a organisationunit
        var getUsersByOrgUnit = function (orgunitID) {
            if (baseApiUrl == null) {
                return getBaseApiUrl().then(
                    function () {
                        return $.getJSON(baseApiUrl + "users?ou=" + orgunitID)
                    },
                    function () {
                        alert("failed to get manifest")
                    }
                );
            }
            return $.getJSON(baseApiUrl + "users?ou=" + orgunitID);
        };
        //Finds information of currentUser
        var getMe = function () {
            if (me == null) {
                if (baseApiUrl == null) {
                    return getBaseApiUrl().then(function () {
                            return $.getJSON(baseApiUrl + "me")
                        })
                        .then(function (json) {
                            me = json;
                            var deffered = $.Deferred();
                            deffered.resolve(json);
                            return deffered.promise();
                        });
                }
                return $.getJSON(baseApiUrl + "me").then(function (json) {
                    me = json;
                });
            }
            var deffered = $.Deferred();
            deffered.resolve(me);
            return deffered.promise();
        };
        //get user by name
        var getUserByName = function (name) {
            if (baseApiUrl == null) {
                return getBaseApiUrl().then(function () {
                    return $.getJSON(baseApiUrl + "users?query=" + name)
                });
            }
            ;
            return $.getJSON(baseApiUrl + "users?query=" + name);
        };
        //implicit constructoR
        //public interface
        return {
            apiBaseUrl: getBaseApiUrl,
            getBaseUrl: getBaseUrl,
            getMeJSON: getMe,
            getUser: getUserById,
            getOrgUnit: loadOrgunitById,
            getUsersOfOrgunit: getUsersByOrgUnit,
            getUserByName: getUserByName
        };
    })();
