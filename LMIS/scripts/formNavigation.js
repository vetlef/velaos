/*
    This module is made for navigating between commodity form
    and the send form
*/
var FormNavigation = (function () {
        //private variables
        var fadeMs = 500;
        var form1 = $("#Form1");
        var form2 = $("#Form2");
        var commodityPagination = $("#pageViewer").children().eq(0);
        var sendPagination = $("#pageViewer").children().eq(1);
        var prevBtn = $("#previousBtn");
        var nextBtn = $("#nextBtn");
        var submitBtn = $("#submitBtn");
        //private functions
        var nextElement = function ( ) {
            if(!(CommodityForm.validateForm()))return false;
            form1.hide();
            form2.fadeIn(fadeMs);
            nextBtn.hide();
            submitBtn.fadeIn(fadeMs);
            prevBtn.fadeIn(fadeMs);   
            commodityPagination.attr("class","disabled");
            sendPagination.attr("class","active");
        };
        var prevElement = function ( ) {
            form2.hide();
            form1.fadeIn(fadeMs);
            prevBtn.hide();
            nextBtn.fadeIn(fadeMs);
            sendPagination.attr("class", "disabled");
            commodityPagination.attr("class", "active");
            submitBtn.hide();
        };
        //controll bindings
        nextBtn.click(nextElement);
        prevBtn.click(prevElement);
        //public functions
        return {
            nextForm: nextElement,
            prevForm: prevElement
        };
    })( );
    /*
        This module is used to navigate between search person form and
        Highermanagement select
    */
    var SendFormNavigation = (function ( ) {
        //private variables
        var fadeMs = 300;
        var personForm = $("#searchPersonForm");
        var higherManagementForm = $("#higherManagementForm");
        var personBtn = $("#bySearchPersonBtn");
        var managementBtn = $("#byHigherManagementBtn");
        var activeForm = higherManagementForm;
        //private functions
        var showPersonForm = function ( ) {
            higherManagementForm.hide();
            personForm.fadeIn(fadeMs);
            managementBtn.attr("class","btn btn-default btn-block btn-sm active");
            personBtn.attr("class","btn btn-primary btn-block btn-sm active");
            activeForm = personForm;
        };
        var showHigherForm = function ( ) {
            personForm.hide();
            higherManagementForm.fadeIn(fadeMs);
            personBtn.attr("class","btn btn-default btn-block btn-sm active");
            managementBtn.attr("class","btn btn-primary btn-block btn-sm active");
            activeForm = higherManagementForm;
        };
        var getActiveForm = function ( ) {
            return activeForm;
        };
        //controll bindings
        personBtn.click(showPersonForm);
        managementBtn.click(showHigherForm);
        //public functions
        return {
            showPersonForm : showPersonForm,
            showHigherManagementForm : showHigherForm,
            getActiveForm : getActiveForm
        };
    })( );

var SendFormValidation = (function ( ) {
    //Private variables
    //private functions
    /*
        Returns the selected id of search-person form
        
        @return the selected id with the type of the id as string(person or orgunit).
                The value is returned as a array
    */
    var getHigherManagementRecipient = function ( ) {
       if ($('#allUsersChk').is(':checked')) { 
            return {
                type: "orgunit",
                id: $("#orgunitSelect").val()
            }
        } 
        return {
            type: "person",
            id: $('#personsSelect').val()
        }
    };
    /*
        Returns the selected id of search-person form
        
        @return the selected id with the type of the id as string(person or orgunit).
                The value is returned as a array
    */
    var getsearchPersonRecipient = function ( ) {
        var id = $('#personsSearchSelect').val()[0];
        return {
            type: "person",
            id: id
        }
    };
    /*
        Finds the selected id in the the current send form, higher management or
        search user. It is important to validate first! Use validation method validate
        decleared in this module.
        
        @return the selected id with the type of the id as string(person or orgunit).
                The value is returned as a object.
    */
    var getRecipientId = function ( ) { 
        var activeForm = SendFormNavigation.getActiveForm().attr("id");
        if(activeForm == "higherManagementForm") return getHigherManagementRecipient();
        else if(activeForm == "searchPersonForm") return getsearchPersonRecipient();
        return null;
    };
    /*
        Validates Higher management form
        @return true if the validation passed, false in all other cases
    */
    var validateHigherManagementForm = function ( ) { 
        var orgUnit = $('#orgunitSelect').val();
        var user = getRecipientId().id;
        if (orgUnit == "#00") {
            alert("Need to choose orgunit!");
            return false;
        }
        else if ($('#allUsersChk').is(':checked')) {
            return true;
        }
        else if (user == "#00") {
            var r = confirm("You did not choose a user to send to. If you want to send to all members in chosen orgunit, you have to check send to all orgunits checkbox");
            if (r == false)
                return false;
        }
        return true;
    };
    /*
        Validates search persons form
        @return true if the validation passed, false in all other cases
    */
    var validatePersonsForm = function ( ){ 
        if($('#personsSearchSelect').val().constructor === Array){
            if($('#personsSearchSelect').val()[0] == "#00") {
                alert("No person selected");
                return false;
            }   
        }else if($('#personsSearchSelect').val() == "#00" ){
            alert("No person selected");
            return false;
        }
        return true;
    };
    /*
        Finds out which form who is selected and validates it by calling 
        either validatePersonsForm or validateHigherManagementForm(). 
        This methods are decleared further up in this module.
        @return true if the validation passed, false in all other cases
    */
    var validate = function ( ) {
        var activeForm = SendFormNavigation.getActiveForm().attr("id");
        if(activeForm == "higherManagementForm") return validateHigherManagementForm();
        else if(activeForm == "searchPersonForm") return validatePersonsForm();
    };
    //public interface
    return {
        getRecipientId : getRecipientId,
        validateForm : validate
    };
})( );