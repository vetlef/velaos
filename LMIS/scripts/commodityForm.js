var CommodityForm = (function () {
    //private Variables
    //private Functions
    var validateForm = function () {
        var myArray = [];
        var nameArr = [];
        var currentArr = [];
        var orderArr = [];

        var commForm = $("#commForm");

        var arr = commForm.serializeArray();

        for (var i = 0; i < arr.length; i++) {
            if (i % 3 == 0) {
                var str = arr[i].value;
                str = str.toLowerCase();
                nameArr.push(str);
            }
            if (i % 3 == 1) {
                currentArr.push(arr[i].value);
            }
            if (i % 3 == 2) {
                orderArr.push(arr[i].value);
            }


            myArray.push(arr[i].value);
        }
        /*Looping through all input to check whether something is empty.*/
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i] == '') {
                alert("You have to fill out all fileds.");
                return;
            }
        }
        /*Looping through all current stock inputs to check whether they are positive whole-numbers*/
        for (var i = 0; i < currentArr.length; i++) {
            if (currentArr[i] < 0) {
                alert("Current Stock must be a non-negative whole number.");
                return;
            }
        }
        /*Looping through all order input to check whether they are positive natural numbers.*/
        for (var i = 0; i < orderArr.length; i++) {
            if (orderArr[i] <= 0) {
                alert("Order quantities must be a non-negative natural number.");
                return;
            }
        }

        function hasDuplicates(nameArr) {
            var valuesSoFar = [];
            for (var i = 0; i < nameArr.length; ++i) {
                var value = nameArr[i];
                if (valuesSoFar.indexOf(value) !== -1) {
                    return true;
                }
                valuesSoFar.push(value);
            }
        };
        if (hasDuplicates(nameArr)) {
            alert("You have added the same commodity more than once.");
            return false;
        }
        else {
            return true;
        }
    };

    //public interface none
    return {
        validateForm: validateForm
    }
})();

var Tools = (function () {
    //private variables
    //private functions
    //public interface
    return {}
})();



var i = 1;

$(function () {
    $(document).on('click', '.btn-add', function (e) {

/*This function is triggered whenever you click the plus button or the minus button on any of the fields.*/

        e.preventDefault();
       
        /*Storing the control form and its first entry in a variable, cloning the first entry.
         Appending the clone to the form from the bottom.
         */
        var controlForm = $('.controls form:first'),
            currentEntry = $('.entry:first'),
            newEntry = $(currentEntry.clone());
        if(CommodityForm.validateForm())
            {
        	newEntry.appendTo(controlForm);
            i++;
            }
        else
            return false;
        

        //Resetting input values in the clone as well as setting the label invisible.
        newEntry.find('input').val('');
        newEntry.find('label').css('visibility','hidden');
        //Changing the appearence and class of the button from plus to minus.

       controlForm.find('.entry:not(:last) .btn-add').remove();
         /*   .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="glyphicon glyphicon-minus"></span>');
          */
        //If the button was a minus button:

    }).on('click', '.btn-remove', function (e) {
    	if(i === 1){
        	return;
        }
        i--;
        
        //Remove the entry.
        $(this).parents('.entry:first').remove();
        //Make sure the top form's label is visible.
        $(".entry:first").find('label').css('visibility', 'visible');
        
        e.preventDefault();
        return false;
    });
});